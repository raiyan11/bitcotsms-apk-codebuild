package com.tritool.bitcotsmsapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.provider.Telephony
import android.util.Log
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.tritool.bitcotsmsapp.rx.RxAPICallHelper
import com.tritool.bitcotsmsapp.rx.RxAPICallback
import io.reactivex.Observable
import org.json.JSONObject


class SmsReceiver : BroadcastReceiver() {

    companion object {
        private val TAG by lazy { SmsReceiver::class.java.simpleName }
    }

    override fun onReceive(context: Context?, intent: Intent?) {

        if (intent?.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            Log.d("ScreenEventReceiver", "ON");
        } else if (intent?.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            Log.d("ScreenEventReceiver", "ON");
        }


        if (!intent?.action.equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) return
        val extractMessages = Telephony.Sms.Intents.getMessagesFromIntent(intent)
        extractMessages.forEach { smsMessage ->
            Log.v(TAG, smsMessage.displayMessageBody)
            context?.let { sendMessage(smsMessage.displayMessageBody, it) }
        }
    }

    private fun sendMessage(text : String,context: Context){
        val mNewsService: NewsService = context.let { ApiProduction(it).provideService(NewsService::class.java) }

        val jsonObject : JSONObject = JSONObject()

        jsonObject.put("text" , text)

        val jsonParser = JsonParser()
        val gsonObject = jsonParser.parse(jsonObject.toString()) as JsonObject

        val apiCall: Observable<String> = mNewsService.getSmsApi(gsonObject)


        RxAPICallHelper().call(apiCall, object : RxAPICallback<String> {
            override fun onSuccess(newsItems: String) {

            }

            override fun onFailed(throwable: Throwable) {

            }
        })

        val intentFilter = IntentFilter()
        intentFilter.addAction(ConnectivityManager.ACTION_RESTRICT_BACKGROUND_CHANGED)
//        context.registerReceiver(this, intentFilter)
        context.applicationContext.registerReceiver(this, intentFilter)

    }



}