package com.tritool.bitcotsmsapp.manager

import android.content.Context
import android.content.ContextWrapper
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.tritool.bitcotsmsapp.R
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.TimeUnit


class RetrofitManager(context: Context) : ContextWrapper(context) {

    companion object {
        private var INSTANCE: RetrofitManager? = null
        private var defaultRetrofit: Retrofit? = null

        fun getInstance(context: Context): RetrofitManager {
            if (INSTANCE == null) {
                initManager(context)
            }
            return INSTANCE!!
        }

        private fun initManager(context: Context) {
            INSTANCE = RetrofitManager(context)

        }
    }

    private fun getDefaultRetrofit():Retrofit{
        if(defaultRetrofit == null){

            val gson = GsonBuilder()

                .create()

            defaultRetrofit = Retrofit.Builder()
                .baseUrl(baseContext.getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
        }

        return defaultRetrofit!!
    }



    fun clearAll() {
        INSTANCE = null
    }


}