package com.tritool.bitcotsmsapp

import android.content.Context
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.pranay.kotlinretrofitapicall.api.OkHttpProduction
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit

class ApiProduction {
    private val BASE_URL:String ="https://hooks.slack.com/services/T038JGHAM/B01EN2LTYJ3/"
    private lateinit var mContext: Context

    constructor(context: Context) {
        this.mContext = context
    }


    private fun provideRestAdapter(): Retrofit {

        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(OkHttpProduction.getOkHttpClient(mContext, true))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    fun <S> provideService(serviceClass: Class<S>): S {
        return provideRestAdapter().create(serviceClass)
    }

}