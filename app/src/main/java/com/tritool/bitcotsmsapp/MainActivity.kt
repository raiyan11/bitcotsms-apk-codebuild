package com.tritool.bitcotsmsapp

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat


class MainActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_CODE_SMS_PERMISSION = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    private fun initViews(){
            val permission = Manifest.permission.RECEIVE_SMS
            val grant = ContextCompat.checkSelfPermission(this, permission)
            if (grant != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(permission), REQUEST_CODE_SMS_PERMISSION)
            }
    }


    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
//        registerActivityLifecycleCallbacks(MyNightDriveAppLifeCycleTracker())
    }

    override fun onDestroy() {
        super.onDestroy()
        val i = Intent(this, SmsReceiver::class.java)
        this.sendBroadcast(i)
//        this.applicationContext.registerReceiver(null, intentFilter)

    }
}